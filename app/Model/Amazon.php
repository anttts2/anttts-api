<?php
App::uses('AppModel', 'Model');
require ROOT . DS.'app/Vendor/vendor/autoload.php';
use Aws\Sns\SnsClient;
use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;
/**
 * Amazon Model
 *
 */
class Amazon extends AppModel {

	public $useTable = false;

	public function get_user_credits($params){ 
			//finy by scholar user id
		try{
			$client = DynamoDbClient::factory(array(
				'credentials' => array(
			        'key'    => Configure::read('aws_key'),
			        'secret' => Configure::read('aws_secret')
		    	),
				'region'  => 'ap-southeast-1'
			));

			$result = $client->getIterator('Scan', array(
                'TableName'     => 'anttts_user_credits',
                'ScanFilter' => array(
                    'user_id' => array(
                        'AttributeValueList' => array(
                            array('N' => $params['user_id'])
                        ),
                        'ComparisonOperator' => 'EQ'
                    )
                ),
                'ScanIndexForward' => false
            ));

			$callback= $result;
		}
		catch(Exception $e){
			$debug = "Exception:{$e}";
			$callback= $debug;
		}

		return($callback);
	}   

	public function save_scholar_withdrawal( $params ){
		$callback = "";
		try{

			//always add this credentials to aws
			$client = DynamoDbClient::factory(array(
				'credentials' => array(
			        'key'    => Configure::read('aws_key'),
			        'secret' => Configure::read('aws_secret')
		    	),
				'region'  => 'ap-southeast-1'
			));
	
			$result = $client->putItem(array(
			    'TableName' => 'anttts_user_credits',
			    'Item' => array(
			    		'user_id' => array('N' => $params['user_id']),
			    		'amount' => array('N' => $params['amount']),
	    				'timestamp'  => array('N' => $params['timestamp']),
	    				'notes'  => array('S' => $params['notes']),
	    				'total'  => array('N' => $params['total']),
			    	)
				));
			$callback= $result;
		}
		catch(Exception $e){
			$debug = "Exception:{$e}";
			$callback= $debug;
		}

		return($callback);
	}

	public function save_donor_deposit( $params ){
		$callback = "";
		try{

			//always add this credentials to aws
			$client = DynamoDbClient::factory(array(
				'credentials' => array(
			        'key'    => Configure::read('aws_key'),
			        'secret' => Configure::read('aws_secret')
		    	),
				'region'  => 'ap-southeast-1'
			));
	
			$result = $client->putItem(array(
			    'TableName' => 'anttts_user_credits',
			    'Item' => array(
			    		'user_id' => array('N' => $params['user_id']),
			    		'amount' => array('N' => $params['amount']),
	    				'timestamp'  => array('N' => $params['timestamp']),
	    				'notes'  => array('S' => $params['notes']),
	    				'total'  => array('N' => $params['total']),
			    	)
				));
			$callback= $result;
		}
		catch(Exception $e){
			$debug = "Exception:{$e}";
			$callback= $debug;
		}

		return($callback);
	}
}