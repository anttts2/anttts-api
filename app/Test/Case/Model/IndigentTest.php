<?php
App::uses('Indigent', 'Model');

/**
 * Indigent Test Case
 */
class IndigentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.indigent',
		'app.ambassador',
		'app.admin',
		'app.institute',
		'app.donor',
		'app.donation'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Indigent = ClassRegistry::init('Indigent');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Indigent);

		parent::tearDown();
	}

}
