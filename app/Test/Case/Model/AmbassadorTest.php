<?php
App::uses('Ambassador', 'Model');

/**
 * Ambassador Test Case
 */
class AmbassadorTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ambassador',
		'app.admin',
		'app.institute',
		'app.indigent'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ambassador = ClassRegistry::init('Ambassador');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ambassador);

		parent::tearDown();
	}

}
