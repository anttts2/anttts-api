<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ApiController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();
	
	public function login() {
		$this->loadModel("User");
		if (!isset($this->request->data['email'])){
			$this->api_result['error_code'] = 'email_required';
			$this->api_result['message'] = 'Email is required.';

			$this->return_result();
		}
		if (!isset($this->request->data['password'])){
			$this->api_result['error_code'] = 'password_required';
			$this->api_result['message'] = 'Password is required.';

			$this->return_result();
		}

		$user = $this->User->findByEmailAndPassword($this->request->data['email'], $this->Auth->password($this->request->data['password']));

		if (!$user){
			$this->api_result['error_code'] = 'authentication_error';
			$this->api_result['message'] = 'Email and password do not match.';

			$this->return_result();
		}

		$this->api_result['data'] = $user;
		$this->api_result['success'] = 1;
		$this->return_result();

	} // public function login()

	public function register() {
		$this->loadModel("User");
		if (!isset($this->request->data['first_name'])){
			$this->api_result['error_code'] = 'firstname_required';
			$this->api_result['message'] = 'First Name is required.';

			$this->return_result();
		}

		if (!isset($this->request->data['last_name'])){
			$this->api_result['error_code'] = 'lastname_required';
			$this->api_result['message'] = 'Last Name is required.';

			$this->return_result();
		}

		if (!isset($this->request->data['email'])){
			$this->api_result['error_code'] = 'email_required';
			$this->api_result['message'] = 'Email is required.';

			$this->return_result();
		}
		if (!isset($this->request->data['password'])){
			$this->api_result['error_code'] = 'password_required';
			$this->api_result['message'] = 'Password is required.';

			$this->return_result();
		}

		if (!filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
			$this->api_result['success'] = 0;
			$this->api_result['error_code'] = 'invalid_email';
			$this->api_result['message'] = 'Invalid email.';

			$this->return_result();
		}

		$user = $this->User->findByEmail($this->request->data['email']);

		if ($user){
			$this->api_result['success'] = 0;
			$this->api_result['error_code'] = 'email_exists';
			$this->api_result['message'] = 'Email already exist.';
		}else{
			// create new user
			$user['User']['email'] = $this->request->data['email'];
			$user['User']['password'] = $this->Auth->password($this->request->data['password']);
			$user['User']['firstname'] = $this->request->data['first_name'];			
			$user['User']['lastname'] = $this->request->data['last_name'];			

			$this->User->create();
			if (!$this->User->save($user)){
				$this->save_error();
			} // if ($this->User->save($user))

			$user = $this->User->findByEmail($this->request->data['email']);
		}		

		$this->api_result['data'] = $user;
		$this->api_result['success'] = 1;

		$this->return_result();

	} // public function register()

	public function get_scholar_credits(){//get scholar data form awss
		if (!isset($this->request->data['user_id'])){
			$this->api_result['error_code'] = 'userid_required';
			$this->api_result['message'] = 'User id is required.';
			$this->return_result();
		}
		$this->loadModel('Amazon');
		$params = $this->request->data;		
		$items = $this->Amazon->get_user_credits($params);
		$user_credits = array();
		foreach ($items as $key => $item) {
			$user_credits = $item;
			// array_push($user_credits, $item);
		}
		$this->api_result['data'] = $user_credits;	
		// var_dump($user_credits);
		$this->return_result();
		// return 123;
	}

	public function get_scholar_user(){//get scholar data form awss
		$this->loadModel("User");
		$this->loadModel("Scholarship");
		if (!isset($this->request->data['email'])){
			$this->api_result['error_code'] = 'email_required';
			$this->api_result['message'] = 'Email is required.';
			$this->return_result();
		}
		$user = $this->User->findByEmail($this->request->data['email']);

		// $user = array();
		// foreach ($items as $key => $item) {
		// 	$user = $item;
		// 	// array_push($user_credits, $item);
		// }
		$scholarship = $this->Scholarship->findByScholarUserId($user["User"]["id"]);
		$this->api_result['data'] = $user["User"];	
		$this->api_result['scholar'] = $scholarship["Scholarship"];	
		// var_dump($user_credits);
		$this->return_result();
		// return 123;
	}

	public function pledge_on_scholar(){
		if (!isset($this->request->data['amount'])){
			$this->api_result['error_code'] = 'amount_required';
			$this->api_result['message'] = 'No amount defined.';
			$this->return_result();
		}
		$this->loadModel("ScholarshipsDonor");
		$scholarship_donor = $this->ScholarshipsDonor->findByDonorUserIdAndScholarshipId($this->request->data['donor_id'], $this->request->data['scholar_id']);
		if($scholarship_donor){
			$this->ScholarshipsDonor->id = $scholarship_donor["ScholarshipsDonor"]["id"];
			$this->ScholarshipsDonor->saveField('cancelled', true);
		}
		else{
			$scholarship_donor['ScholarshipsDonor']['donor_user_id'] = $this->request->data['donor_id'];
			$scholarship_donor['ScholarshipsDonor']['scholarship_id'] = $this->request->data['scholar_id'];
			$scholarship_donor['ScholarshipsDonor']['amount_pledged'] = $this->request->data['amount'];			
			$scholarship_donor['ScholarshipsDonor']['last_active_date'] = date('Y-m-d');			
			$scholarship_donor['ScholarshipsDonor']['cancelled'] = false;	

			$this->ScholarshipsDonor->create();
			if (!$this->ScholarshipsDonor->save($scholarship_donor)){
				$this->save_error();
			} // if ($this->User->save($user))
		}

		$this->api_result['success'] = 1;
		$this->api_result['data'] = $scholarship_donor;	
		$this->return_result();
	}
	
	//for withdrawal
	public function scholar_withdrawal(){
		if (!isset($this->request->data['user_id'])){
			$this->api_result['error_code'] = 'userid_required';
			$this->api_result['message'] = 'User id is required.';
			$this->return_result();
		}
		if (!isset($this->request->data['amount'])){
			$this->api_result['error_code'] = 'amount_required';
			$this->api_result['message'] = 'Amount is required.';
			$this->return_result();
		}

		try{
			$this->loadModel('Amazon');
			$params = $this->request->data;		
			
			//query the latest timestamp of user in user_credits table and get the total of that
			$latest_user_credits_total = $this->Amazon->get_user_credits($params);
			$temp = array();
			foreach ($latest_user_credits_total as $key => $item) {
				if($item){
					$temp = $item;
				}
			} 
			
			if($temp != []){
				$params['total'] = $temp['total']['N'] - $this->request->data['amount'];
			}else{
				$params['total'] = $this->request->data['amount'];
			}

			$items = $this->Amazon->save_scholar_withdrawal($params);
			
			// var_dump($user_credits);
			$this->api_result['success'] = 1;
		}catch(Exception $e){
			$this->api_result['success'] = 0;
			$this->api_result['message'] = $e;
		}
		
		$this->return_result();
	}

	//for donor deposit
	public function donor_deposit(){
		if (!isset($this->request->data['user_id'])){
			$this->api_result['error_code'] = 'userid_required';
			$this->api_result['message'] = 'User id is required.';
			$this->return_result();
		}
		if (!isset($this->request->data['amount'])){
			$this->api_result['error_code'] = 'amount_required';
			$this->api_result['message'] = 'Amount is required.';
			$this->return_result();
		}

		try{
			$this->loadModel('Amazon');
			$params = $this->request->data;		

			//query the latest timestamp of user in user_credits table and get the total of that
			$latest_user_credits_total = $this->Amazon->get_user_credits($params);
			$temp = array();
			foreach ($latest_user_credits_total as $key => $item) {
				if($item){
					$temp = $item;
				}
			} 
			
			if($temp != []){
				$params['total'] = $temp['total']['N'] + $this->request->data['amount'];
			}else{
				$params['total'] = $this->request->data['amount'];
			}
			
			$items = $this->Amazon->save_donor_deposit($params);
			
			// var_dump($user_credits);
			$this->api_result['success'] = 1;
		}catch(Exception $e){
			$this->api_result['success'] = 0;
			$this->api_result['message'] = $e;
		}
		
		$this->return_result();
	}

}