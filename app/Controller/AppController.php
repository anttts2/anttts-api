<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	var $components = array('Auth');

	var $api_result = array(
		'success' => 0,
		);
	var $valid_requests = array('post', 'put', 'get');

	public function beforeFilter(){

		parent::beforeFilter();

		$this->Auth->allow();

		// if data is sent as GET (for testing)
		if ($this->request->query){
			$this->request->data = array_merge($this->request->data, $this->request->query);
		}

	}

	function beforeRender(){
		// print_r($this->api_result);

		// echo json_encode($this->api_result);
		// exit;
	}



////////////////////////////////////////////////

	function invalid_request(){
		$this->api_result['success'] = 0;
		$this->api_result['error_code'] = 'invalid_request';
		$this->api_result['message'] = 'Invalid request.';

		$this->return_result();
	}

	function invalid_email(){
		$this->api_result['success'] = 0;
		$this->api_result['error_code'] = 'email_invalid';
		$this->api_result['message'] = 'Email is invalid.';

		$this->return_result(); 
	}

	function email_exist(){
		$this->api_result['success'] = 0;
		$this->api_result['error_code'] = 'email_exist';
		$this->api_result['message'] = 'Email already exist.';

		$this->return_result();
	}

	function invalid_user(){
		$this->api_result['success'] = 0;
		$this->api_result['error_code'] = 'invalid_user';
		$this->api_result['message'] = 'Invalid user.';

		$this->return_result();
	}

	function store_access_denied(){
		$this->api_result['success'] = 0;
		$this->api_result['error_code'] = 'store_access_denied';
		$this->api_result['message'] = 'Store access denied.';

		$this->return_result();
	}

	function invalid_token(){
		$this->api_result['success'] = 0;
		$this->api_result['error_code'] = 'invalid_token';
		$this->api_result['message'] = 'Invalid token.';

		$this->return_result();
	}

	function no_items_found(){
		$this->api_result['success'] = 0;
		$this->api_result['error_code'] = 'no_items_found';
		$this->api_result['message'] = 'No items found.';

		$this->return_result();
	}

	function save_error(){
		$this->api_result['success'] = 0;
		$this->api_result['error_code'] = 'save_error';
		$this->api_result['message'] = 'Error saving data. Please try again.';

		$this->return_result();
	}

	function authentication_error(){
		$this->api_result['success'] = 0;
		$this->api_result['error_code'] = 'authentication_error';
		$this->api_result['message'] = 'User authentication error.';

		$this->return_result();
	}

	function return_result(){
		if (isset($_GET['test'])){
			print_r($this->api_result);
		}
		echo json_encode($this->api_result);
		exit;
	}
////////////////////////////////////////////////








	public function temp($input=''){
		echo $this->Auth->password('123');
		exit;

		$this->api_result['data']['password'] = $this->Auth->password($input);
		$this->api_result['data']['encrypt'] = Security::encrypt($input, Configure::read('encryption_key'));

		$this->return_result();
	}

	public function temp2() {
		App::uses('CakeEmail', 'Network/Email');

		$email = new CakeEmail();
		$email->config('mandrill');
		// $email->from(array('me@example.com' => 'My Site'));
		$email->to('theisoft@gmail.com');
		$email->subject('Anttts');
		$result = $email->send('My message2');

		print_r($result);
		exit;
	}
}
